# @escio/stream-buffer-until-end

> Duplex object stream buffering data until end of the stream and then letting all though.

This is useful for cases where you want to wait for the part of a stream pipeline to clear before passing data on to the rest of the pipeline.

## Installation

```
$ npm install @escio/stream-buffer-until-end
```

## Usage

The most common use-case is in a gulp pipeline where you want to wait until all files have been written to disk before triggering a livereload, or similar.

```javascript
const gulp = require('gulp');
const livereload = require('gulp-livereload')
const bufferUntilEnd = require('@escio/stream-buffer-until-end');

gulp.task('build', () => gulp.src(
    'sourceFiles'
).pipe(
    ...transform data...
).pipe(
    gulp.dest('targetDirectory')
).pipe(
    // No files will pass this point until the gulp.dest stream is done
    // writing to disk and sends the end signal...
    bufferUntilEnd()
).pipe(
    livereload()
));
```

## License

ISC © Escio AS
