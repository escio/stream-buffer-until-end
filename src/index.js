'use strict';

const duplexify = require('duplexify');
const stream = require('stream');
const StreamFromArray = require('stream-from-array');
const util = require('gulp-util');

module.exports = () => {
	const source = new stream.PassThrough({
		objectMode: true
	});
	const destination = new stream.PassThrough({
		objectMode: true
	});

	source.pipe(
		util.buffer(function (err, objects) {
			if (err) {
				destination.emit('error', err);
				return;
			}

			StreamFromArray.obj(
				objects
			).pipe(
				destination
			);
		})
	);

	return duplexify(source, destination, {
		objectMode: true
	});
};
